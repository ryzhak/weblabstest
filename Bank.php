<?php

namespace frontend\models;

use common\components\EventHandler;
use Yii;
use yii\base\ErrorException;

/**
 * This is the model class for table "bank".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $money_cat_id
 * @property string $amount
 *
 * @property TableMoneyCat $moneyCat
 * @property User $user
 */
class Bank extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'money_cat_id'], 'integer'],
            [['amount'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'money_cat_id' => Yii::t('app', 'Money Cat ID'),
            'amount' => Yii::t('app', 'Amount'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMoneyCat()
    {
        return $this->hasOne(TableMoneyCat::className(), ['id' => 'money_cat_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * Возвращает сумму кона
     *
     * @param integer $action_history_id id экшена
     * @param boolean $minus_last_azi вычитать или нет из суммы кона сумму последних ставок для участия в АЗИ раунде
     * @param boolean $with_blinds учитывать ли блайнды при подсчете суммы кона
     * @return decimal сумма на кону
     * @throws ErrorException
     */
    public static function getKonSum($action_history_id, $minus_last_azi = false, $with_blinds = false){

        if(!$mTableActionHistory = TableActionHistory::findOne($action_history_id)){
            throw new ErrorException("Model TableActionHistory with id $action_history_id not found");
        }

        $konSum = 0;

        $mGameHistory = GameHistory::findOne($mTableActionHistory->gamePart->game_history_id);

        $mTableOnline = TablesOnline::findOne($mGameHistory->table_id);

        //перебираем все игры, обычные и АЗИ если есть
        foreach($mGameHistory->gameParts as $mGamePart){
            //перебираем все раунды в игре
            foreach($mGamePart->tableActionHistories as $mTAH){
                //если торги или ази раунд, то пробегаем по мувам и высчитываем сумму
                if($mTAH->action_id == EventHandler::ROUND_1_TRADING){

                    //для всех игроков, участвующих в торгах
                    foreach($mTAH->userStates as $mUserState){

                        if($mLastMove = MoveHistory::find()->where("action_history_id = :action_history_id AND user_id = :user_id AND (move_type_id = :player_raise OR move_type_id = :player_call)",[
                            ':action_history_id' => $mTAH->id,
                            ':user_id' => $mUserState->user_id,
                            ':player_raise' => EventHandler::PLAYER_RAISE,
                            ':player_call' => EventHandler::PLAYER_CALL
                        ])->orderBy('id DESC')->one()){

                            $konSum += $mLastMove->sum;
                            continue;

                        }

                        //прибавляем блайнд для каждого игрока
                        if($with_blinds) $konSum += self::getBlindSum($mTableOnline->cat_id);

                    }

                }
                if($mTAH->action_id == EventHandler::AZI_PREPARE){
                    foreach($mTAH->moveHistories as $mMoveAZI){
                        $konSum += $mMoveAZI->sum;
                    }
                }
            }
        }

        //если вычитаем последние ставки для АЗИ
        if($minus_last_azi){
            //ищем последний экшн AZI_PREPARE
            $lastGameParts = GamePart::find()->where(['game_history_id' => $mGameHistory->id])->orderBy('id DESC')->all();

            foreach($lastGameParts as $mLastGamePart){

                if($mLastAZIPrepareAction = TableActionHistory::find()->where([
                    'game_part_id' => $mLastGamePart->id,
                    'action_id' => EventHandler::AZI_PREPARE
                ])->one()){

                    $mAZIBets = MoveHistory::findAll([
                        'action_history_id' => $mLastAZIPrepareAction->id
                    ]);
                    foreach($mAZIBets as $mAZIBet){
                        $konSum -= $mAZIBet->sum;
                    }

                    break;

                }

            }

        }

        return $konSum;
    }

    /**
     * Вычитает сумму из банка игрока
     *
     * @param integer $user_id id игрока
     * @param integer $money_cat_id категория денег
     * @param decimal $sum сумма
     * @throws ErrorException
     */
    public static function minus($user_id, $money_cat_id, $sum){
        if(!$mBank = self::findOne([
            'user_id' => $user_id,
            'money_cat_id' => $money_cat_id
        ])){
            throw new ErrorException('Can not find model Bank in minus()');
        }
        $mBank->amount -= $sum;
        if(!$mBank->save()){
            throw new ErrorException('Can not save model Bank in minus()');
        }
    }

    /**
     * Добавляет сумму в банк игроку
     *
     * @param integer $user_id id игрока
     * @param integer $money_cat_id категория денег
     * @param decimal $sum сумма
     * @throws ErrorException
     */
    public static function plus($user_id, $money_cat_id, $sum){
        if(!$mBank = self::findOne([
            'user_id' => $user_id,
            'money_cat_id' => $money_cat_id
        ])){
            throw new ErrorException('Can not find model Bank in minus()');
        }
        $mBank->amount += $sum;
        if(!$mBank->save()){
            throw new ErrorException('Can not save model Bank in minus()');
        }
    }

    /**
     * Обновляет банки игроков
     *
     * @param integer $action_history_id id экшена
     * @throws ErrorException
     */
    public static function updateAmounts($action_history_id){

        if(!$mAction = TableActionHistory::findOne($action_history_id)){
            throw new ErrorException('Can not find model TableActionHistory in updateAmounts()');
        }

        $mTableOnline = TablesOnline::findOne($mAction->gamePart->gameHistory->table_id);

        if($mAction->action_id == EventHandler::ROUND_1_TRADING){

            //для каждого игрока участвовавшего в торгах
            foreach($mAction->userStates as $mUserState){

                //если есть рейз или колл
                if($mLastMove = MoveHistory::find()->where("action_history_id = :action_history_id AND user_id = :user_id AND (move_type_id = :player_raise OR move_type_id = :player_call)",[
                    ':action_history_id' => $action_history_id,
                    ':user_id' => $mUserState->user_id,
                    ':player_raise' => EventHandler::PLAYER_RAISE,
                    ':player_call' => EventHandler::PLAYER_CALL
                ])->orderBy('id DESC')->one()){
                    self::minus($mUserState->user_id, $mTableOnline->money_cat_id, $mLastMove->sum);
                } else{
                    //есть только пас
                    self::minus($mUserState->user_id, $mTableOnline->money_cat_id, self::getBlindSum($mTableOnline->cat_id));
                }
            }
        }

        if($mAction->action_id == EventHandler::AZI_PREPARE){
            foreach($mAction->moveHistories as $mAZIMove){
                Bank::minus($mAZIMove->user_id, $mTableOnline->money_cat_id, $mAZIMove->sum);
            }
        }

    }

    /**
     * Возвращает общую сумму блайндов для экшена
     *
     * @param TableActionHistory $action_history_id id экшена
     * @return decimal сумма блайндов
     * @throws ErrorException
     */
    public static function getBlindsSumForAction($action_history_id){
        $mAction = TableActionHistory::findOne($action_history_id);
        $mTable = TablesOnline::findOne($mAction->gamePart->gameHistory->table_id);
        $blind = self::getBlindSum($mTable->cat_id);
        return $mAction->getUserStates()->count() * $blind;
    }

    /**
     * По категории стола возвращает сумму 1 блайнда
     *
     * @param $cat_id integer категория стола, 1 - beginner, 2 - pro, 3 - VIP
     * @return decimal сумма 1 блайнда
     * @throws ErrorException
     */
    public static function getBlindSum($cat_id){
        switch($cat_id){
            case EventHandler::CAT_BEGINNER:
                return EventHandler::BEGINNER_MIN_KON;
            case EventHandler::CAT_PROFESSIONAL:
                return EventHandler::PRO_MIN_KON;
            case EventHandler::CAT_VIP:
                return EventHandler::VIP_MIN_KON;
            default:
                throw new ErrorException("CAT ID not found in getBlindSum()");
        }
    }

    /**
     * Проверяет есть ли у игрока необходимая сумма денег для ставки
     *
     * @param MoveHistory $mMoveHistory id мува
     * @return boolean есть ли у игрока необходимая сумма денег
     */
    public static function userHasMoney($mMoveHistory){
        $mTableOnline = TablesOnline::findOne($mMoveHistory->actionHistory->gamePart->gameHistory->table_id);
        $mBank = self::findOne([
            'user_id' => $mMoveHistory->user_id,
            'money_cat_id' => $mTableOnline->money_cat_id
        ]);

        return round($mBank->amount - $mMoveHistory->sum, 2) >= 0;
    }

    /**
     * По категории стола возвращает максимальную ставку
     *
     * @param $cat_id integer категория стола, 1 - beginner, 2 - pro, 3 - VIP
     * @return decimal максимальная ставка
     * @throws ErrorException
     */
    public static function getMaxBetSum($cat_id){
        switch($cat_id){
            case EventHandler::CAT_BEGINNER:
                return EventHandler::BEGINNER_MAX_BET;
            case EventHandler::CAT_PROFESSIONAL:
                return EventHandler::PRO_MAX_BET;
            case EventHandler::CAT_VIP:
                return EventHandler::VIP_MAX_BET;
            default:
                throw new ErrorException("CAT ID not found in getMaxBetSum()");
        }
    }

    /**
     * По категории стола возвращает минимальную ставку
     *
     * @param $cat_id integer категория стола, 1 - beginner, 2 - pro, 3 - VIP
     * @return decimal минимальная ставка
     * @throws ErrorException
     */
    public static function getMinBetSum($cat_id){
        switch($cat_id){
            case EventHandler::CAT_BEGINNER:
                return EventHandler::BEGINNER_MIN_BET;
            case EventHandler::CAT_PROFESSIONAL:
                return EventHandler::PRO_MIN_BET;
            case EventHandler::CAT_VIP:
                return EventHandler::VIP_MIN_BET;
            default:
                throw new ErrorException("CAT ID not found in getMinBetSum()");
        }
    }

    /**
     * Возвращает текущую мин ставку для раунда торгов
     *
     * @param $action_history_id
     * @return double мин ставка для раунда торгов
     */
    public static function getCurrentMinBet($action_history_id){
        //ищем раунд торгов
        $mTradeAction = TableActionHistory::findOne($action_history_id);
        $mTable = TablesOnline::findOne($mTradeAction->gamePart->gameHistory->table_id);
        $minBetForTable = self::getMinBetSum($mTable->cat_id); //0.1
        //ищем последний рейз
        if($mLastRaiseMove = MoveHistory::find()->where(
            'action_history_id = :action_history_id AND move_type_id = :move_type_id',[
            ':action_history_id' => $mTradeAction->id,
            ':move_type_id' => EventHandler::PLAYER_RAISE
        ])->orderBy('id DESC')->one()){
            $minBetForTable = $mLastRaiseMove->sum;
        }
        //прибавляем блайнд
        return $minBetForTable + self::getMinBetSum($mTable->cat_id);
    }

    /**
     * Возвращает текущую max ставку
     *
     * @param $action_history_id
     * @return double текущая max ставка
     */
    public static function getCurrentMaxBet($action_history_id){

        $minBet = self::getCurrentMinBet($action_history_id); //0.1

        $mTradeAction = TableActionHistory::findOne($action_history_id);
        $mTable = TablesOnline::findOne($mTradeAction->gamePart->gameHistory->table_id);
        $maxBet = self::getMaxBetSum($mTable->cat_id);

        return $minBet + $maxBet - self::getMinBetSum($mTable->cat_id);

    }

    /**
     * Возвращает сумму на кону перед началом раунда торгов без учета последний блайндов
     *
     * @param $action_history_id
     * @param $with_last_blinds
     * @return double сумма на кону перед началом раунда торгов без учета последний блайндов
     * @throws ErrorException
     */
    public static function getSumBeforeTrade($action_history_id, $with_last_blinds = true){

        if(!$mTableActionHistory = TableActionHistory::findOne($action_history_id)){
            throw new ErrorException("Model TableActionHistory with id $action_history_id not found");
        }

        $konSum = 0;

        $mGameHistory = GameHistory::findOne($mTableActionHistory->gamePart->game_history_id);
        //находим блайнды
        $mTableOnline = TablesOnline::findOne($mGameHistory->table_id);
        $blind = self::getBlindSum($mTableOnline->cat_id);

        //перебираем все игры, обычные и АЗИ если есть
        foreach($mGameHistory->gameParts as $mGamePart){
            //перебираем все раунды в игре
            foreach($mGamePart->tableActionHistories as $mTAH){

                if($mTAH->id >= $action_history_id) continue;

                //если торги или ази раунд, то пробегаем по мувам и высчитываем сумму
                if($mTAH->action_id == EventHandler::ROUND_1_TRADING){

                    //для всех игроков, участвующих в торгах
                    foreach($mTAH->userStates as $mUserState){

                        if($mLastMove = MoveHistory::find()->where([
                            'action_history_id' => $mTAH->id,
                            'user_id' => $mUserState->user_id
                        ])->orderBy('id DESC')->one()){

                            $konSum += $mLastMove->sum;

                        }

                    }

                    //добавляем блайнды
                    if($with_last_blinds){
                        $konSum += count(TableActionHistory::getUserIDsInFirstTradeRound($action_history_id)) * $blind;
                    }

                }

                if($mTAH->action_id == EventHandler::AZI_PREPARE){
                    foreach($mTAH->moveHistories as $mMoveAZI){
                        $konSum += $mMoveAZI->sum;
                    }
                }

            }
        }

        return $konSum;

    }

    /**
     * Возвращает текущую сумму в банке игрока после вычетов в текущем раунде торгов
     *
     * @param $userId
     * @param $lastActionHistoryId
     * @return decimal
     */
    public static function getCurrentUserBankAmount($userId, $lastActionHistoryId){

        $mTableOnline = TablesOnline::findOne(TableActionHistory::findOne($lastActionHistoryId)->gamePart->gameHistory->table_id);
        $mBank = self::findOne([
            'user_id' => $userId,
            'money_cat_id' => $mTableOnline->money_cat_id
        ]);

        $betSum = $mBank->amount;

        //если в текущем gamePart нет нового экшена, то вычитаем
        $mActionHistory = TableActionHistory::findOne($lastActionHistoryId);
        if(!$mNewActionHistory = TableActionHistory::find()->where("game_part_id = :game_part_id AND id > :id", [
            ':game_part_id' => $mActionHistory->game_part_id,
            ':id' => $lastActionHistoryId
        ])->one()){

            if($mLastRaiseOrCall = MoveHistory::find()->where("action_history_id = :action_history_id AND user_id = :user_id AND (move_type_id = :player_raise OR move_type_id = :player_call)",[
                "action_history_id" => $lastActionHistoryId,
                "user_id" => $userId,
                "player_raise" => EventHandler::PLAYER_RAISE,
                "player_call" => EventHandler::PLAYER_CALL
            ])->orderBy('id DESC')->one()){
                $betSum -= $mLastRaiseOrCall->sum;
            }

        }

        return $betSum;

    }


}
