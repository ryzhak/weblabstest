<?php

namespace app\components;
use SimpleHtmlDom as SHD;

class Livetvparser 
{
    public $parseURL = "";
    public $domain = "http://livetv.sx";
    public $timeWall;
    
    public function __construct($url, $until = "nextDay") {
        
        $this->parseURL = $url;
        
        $now = date("Y-m-d H:i:s");
        if($until == "nextDay"){
            $next = date("Y-m-d H:i:s", strtotime($now . "+ 1 day"));
        } elseif ($until == "nextYear") {
            $next = date("Y-m-d H:i:s", strtotime($now . "+ 1 year"));
        }
        $this->timeWall = strtotime($next);
        
    }


    public function parseStreams(){

        $conn = \yii::$app->streamDB;
        
        $ch = curl_init($this->parseURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, '');
        $data = curl_exec($ch);
        $html = SHD\str_get_html($data);

        //номера таблиц на странице, меняются в зависимости от дня
        $tblIndexes = [7, 6];

        foreach($tblIndexes as $tblIndex){

            $scoreTable = $html->find("table[align=center]", $tblIndex);
            $tables = $scoreTable->find("td table");

            $i = 0;
            foreach ($tables as $table){

                $i++;
                //если первая таблица (с картинкой) то пропускаем
                if ($i == 1) continue;

                //ищем чемпионат и дату
                $champAndDate = trim($table->find("td span.evdesc", 0)->plaintext);
                $elements = explode("\n", $champAndDate);
                //ищем чемпионат
                $championat = trim($elements[1]);
                //вырезаем скобки
                $championat[0] = "";
                $championat[strlen($championat) - 1] = "";
                //echo $championat . "<br />";

                //ищем дату
                $date = $elements[0];
                $date = str_replace(" at", "", $date);
                //находим первый пробел
                $firstBlank = strpos($date, " ");
                //находим второй пробел
                $secondBlank = strpos($date, " ", $firstBlank + 1);
                //добавляем год
                $date = substr_replace($date, " " . date('Y'), $secondBlank, 0);
                $date = strtotime($date);

                //если дата игры больше установенной, то заново
                if($date > $this->timeWall) continue;

                //форматируем дату под mysql
                $date = date("Y-m-d H:i:s", $date) . "<br />";

                //url со ссылкой на игру команд
                $gameURL = $table->find('td a',0)->href;

                //ищем названия команд
                $teams = $table->find('td a',0)->plaintext;
                //если есть тирэ то обычная игра
                if(strpos($teams, "&ndash;")){
                    $teams = explode("&ndash;", $teams);
                    $teamHome = trim($teams[0]);
                    $teamAway = trim($teams[1]);
                } else {
                    //иначе симулкаст
                    $teamHome = trim($teams);
                    $teamAway = trim($teams);
                }
                //echo $teamHome . " $teamAway" . "<br />";


                //ищем счет
                //TODO: добавить проверку на дату, не обновлять счет если дата прошлая(если счет есть то обновляем)
                $score = $table->find("td span.live", 0);
                if(empty($score) || preg_match('/[0-9]/', $score) == 0) {
                    //ниче не делаем
                } else {
                    $score = trim(str_replace("&nbsp;", "", $score->plaintext));
                    $goalsHomeTeam = trim($score[0]);
                    $goalsAwayTeam = trim($score[2]);
                }


                //сохраняем результаты
                //проверяем есть ли в БД такой чемпионат и сохраняем
                $command = $conn->createCommand("select * from {{%championship}} where name = :name");
                $command->bindParam(":name", $championat);
                $mChampionat = $command->queryOne();
                if(empty($mChampionat)){
                    $conn->createCommand()->insert('{{%championship}}', [
                        'name' => $championat,
                    ])->execute();
                    $mChampionat = $command->queryOne();
                }

                //проверяем есть ли в БД такие команды
                //домашняя
                $command = $conn->createCommand("select * from {{%stream_team}} where name = :name");
                $command->bindParam(":name", $teamHome);
                $mTeamHome = $command->queryOne();
                if(empty($mTeamHome)){
                    $conn->createCommand()->insert('{{%stream_team}}', [
                        'name' => $teamHome,
                        //'championship_id' => $mChampionat['id'],
                    ])->execute();
                    $mTeamHome = $command->queryOne();
                }
                //гостевая
                $command = $conn->createCommand("select * from {{%stream_team}} where name = :name");
                $command->bindParam(":name", $teamAway);
                $mTeamAway = $command->queryOne();
                if(empty($mTeamAway)){
                    $conn->createCommand()->insert('{{%stream_team}}', [
                        'name' => $teamAway,
                    ])->execute();
                    $mTeamAway = $command->queryOne();
                }

                //проверяем есть ли игра в бд
                $command = $conn->createCommand("select * from {{%game}} where team_id_home = :team_id_home"
                    . " AND team_id_away = :team_id_away AND game_date = :game_date");
                $command->bindParam(":team_id_home", $mTeamHome['id']);
                $command->bindParam(":team_id_away", $mTeamAway['id']);
                $command->bindParam(":game_date", $date);
                $mGame = $command->queryOne();
                if(empty($mGame)){
                    //если лайв
                    if(isset($goalsHomeTeam) && isset($goalsAwayTeam)){
                        $conn->createCommand()->insert('{{%game}}', [
                            'championship_id' => $mChampionat['id'],
                            'team_id_home' => $mTeamHome['id'],
                            'team_id_away' => $mTeamAway['id'],
                            'goals_home' => $goalsHomeTeam,
                            'goals_away' => $goalsAwayTeam,
                            'game_date' => $date,
                        ])->execute();
                    } else {
                        $conn->createCommand()->insert('{{%game}}', [
                            'championship_id' => $mChampionat['id'],
                            'team_id_home' => $mTeamHome['id'],
                            'team_id_away' => $mTeamAway['id'],
                            'game_date' => $date,
                        ])->execute();
                    }
                } else {
                    //обновляем игру
                    //если лайв
                    if(isset($goalsHomeTeam) && isset($goalsAwayTeam)){
                        $command = $conn->createCommand("UPDATE {{%game}} SET goals_home = :goals_home, goals_away = :goals_away WHERE id = :id");
                        $command->bindParam(":goals_home", $goalsHomeTeam);
                        $command->bindParam(":goals_away", $goalsAwayTeam);
                        $command->bindParam(":id", $mGame['id']);
                        $command->execute();
                    }
                }

                //заново находим игру
                $command = $conn->createCommand("select * from {{%game}} where team_id_home = :team_id_home"
                    . " AND team_id_away = :team_id_away AND game_date = :game_date");
                $command->bindParam(":team_id_home", $mTeamHome['id']);
                $command->bindParam(":team_id_away", $mTeamAway['id']);
                $command->bindParam(":game_date", $date);
                $mGame = $command->queryOne();

                //сохраняем ссылки
                $this->saveLinks($this->domain . $gameURL, $mGame);

                unset($goalsHomeTeam);
                unset($goalsAwayTeam);

            }

        }


    }
    
    /**
     * По ссылке на игру с сайта livetv.sx сохраняет стримы на игру
     * 
     * @param array $mGame массив значение полей из s_game
     * @param string $gameURL ссылка на страницу игры
     */
    public function saveLinks($gameURL, $mGame){
        $conn = \yii::$app->streamDB;
        
        $ch = curl_init($gameURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
        $data = curl_exec($ch);
        $html = SHD\str_get_html($data);
        
        //если на странице есть ссылки, то парсим иначе возвращаемся
        if($html->find("div[id=links_block] table", 0)){
            $linkBlock = $html->find("div[id=links_block]", 0);
        } else {
            //die("not found");
            return;
        }

        //таблица со ссылками для браузера
        if($linkBlock->find("table table",0)){
            $browserLinksTable = $linkBlock->find("table table",0);
            //ищем веб ссылки
            $trs = $browserLinksTable->find("tr");
            $i = 0;
            foreach($trs as $tr){
                $i++;
                if($i % 2 == 0) continue;
                //язык
                if($tr->find(".date",0)){
                    $lang = trim($tr->find(".date",0)->plaintext);
                } else {
                    $lang = "";
                }
                //ссылка
                if($tr->find("table a", 1)){
                    $weblink = trim($tr->find("table a", 1)->href);
                } else {
                    $weblink = "no";
                }
                //echo $lang . " " . $weblink . "<br />";
                
                $this->saveLinkToDB($mGame, 1, $weblink, $lang);
            }
        }
        
        //таблица со ссылками для сопок
        $wlTable = $linkBlock->find("table", 0);
        $wltc = count($wlTable->find("table"));
        if($linkBlock->find("table", $wltc + 1)){
            $sopLinksTable = $linkBlock->find("table", $wltc + 1);
            //ищем сопки и ссылки с торрентов
            $trs = $sopLinksTable->find("tr table tr");
            $i = 0;
            foreach($trs as $tr){
                $i++;
                if($i % 2 == 0) continue;
                //язык
                if($tr->find(".date",0)){
                    $lang = trim($tr->find(".date",0)->plaintext);
                } else {
                    $lang = "";
                }
                //ссылка
                if($tr->find("td[width=15] a", 0)){
                    $a = trim($tr->find("td[width=15] a", 0)->href);
                } else {
                    $a = $tr->find("table a", 1)->href;
                }
                //echo $lang . " " . $a . "<br />";
                
                $type = $tr->find("td" ,1)->plaintext;
                $firstBlank = strpos($type, " ");
                $type = trim(substr($type, $firstBlank));
                
                if($type == "SopCast"){
                    $this->saveLinkToDB($mGame, 2, $a, $lang);
                } elseif ($type == "TrntStrm") {
                    $this->saveLinkToDB($mGame, 3, $a, $lang);
                }
                
            }
        }
        
        
        //скрытые веб ссылки
        if($linkBlock->find("div[id='hiddenlinks1'] table", 0)){
            $hiddenWebLinks = $linkBlock->find("div[id='hiddenlinks1'] table", 0);
            $trs = $hiddenWebLinks->find("tr");
            $i = 0;
            foreach($trs as $tr){
                $i++;
                if($i % 2 == 0) continue;
                //язык
                if($tr->find(".date",0)){
                    $lang = trim($tr->find(".date",0)->plaintext);
                } else {
                    $lang = "";
                }
                //ссылка
                if($tr->find("table a", 1)){
                    $weblink = trim($tr->find("table a", 1)->href);
                } else {
                    $weblink = "no";
                }
                //echo $lang . " " . $weblink . "<br />";
                $this->saveLinkToDB($mGame, 1, $weblink, $lang);

            }
        }
     
    }
    
    /**
     * Сохраняет ссылку в БД
     * 
     * @param array $mGame массив со значениями полей таблицы игр
     * @param integer $linkTypeID тип ссылки
     * @param string $link ссылка
     * @param string $lang язык
     */
    public function saveLinkToDB($mGame, $linkTypeID, $link, $lang){

        //если веб ссылка, то проверяем есть ли http в адресе, иначе добавляем
        if($linkTypeID == 1){
            if(strpos($link, 'http://') === false){
                $link = 'http://cdn.livetv.sx' . $link;
            }
        }

        $conn = \yii::$app->streamDB;
        //проверяем есть ли уже такая ссылка для данной игры
        $command = $conn->createCommand("select * from {{%link}} where game_id = :game_id AND link = :link");
        $command->bindParam(":game_id", $mGame['id']);
        $command->bindParam(":link", $link);
        $mLink = $command->queryOne();
        if(empty($mLink)){
            
            //проверяем есть ли такой язык
            $command = $conn->createCommand("select * from {{%link_language}} where name = :name");
            $command->bindParam(":name", $lang);
            $mLang = $command->queryOne();
            if(empty($mLang)){
                $conn->createCommand()->insert('{{%link_language}}', [
                'name' => $lang,
                ])->execute();
                $mLang = $command->queryOne();
            }
            
            //вставляем ссылку
            $conn->createCommand()->insert('{{%link}}', [
                'game_id' => $mGame['id'],
                'link_type_id' => $linkTypeID,
                'link' => $link,
                'lang_id' => $mLang['id'],
                'link_source_id' => 1,
            ])->execute();
            
            
        } else {
            return;
        }
          
    }
    
}
