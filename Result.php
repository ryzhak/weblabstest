<?php

namespace app\models;

use Yii;
use yii\data\ArrayDataProvider;
use app\assets\BetPriority;
use app\models\Stats;
use yii\db\Query;
use app\assets\Translator;

/**
 * This is the model class for table "tbl_result".
 *
 * @property integer $id
 * @property integer $team_id_home
 * @property integer $team_id_away
 * @property integer $goals_home
 * @property integer $goals_away
 * @property integer $predict_goals_home
 * @property integer $predict_goals_away
 *
 * @property Team $teamIdHome
 * @property Team $teamIdAway
 */
class Result extends \yii\db\ActiveRecord
{
    /**
     * @var double процент для тестовой выборки от общего количества игр в сезоне(0.3 = 30%) 
     */
    public static $testPct = 0.3;

    public static $leagesIDsForTotal = [1,2,3,5,6,7,8,9,11,13,14];

    public static $coefs = [
        'totalover25' => 1.9,
        'hometotal15' => 1.7,
        'x2' => 1.65,
        'hometoscoreyes' => 1.2,
        'x1' => 1.3,
        'totalover15' => 1.25,
        'win2' => 1.9,
        'awaytoscoreyes' => 1.4,
        'totalunder35' => 1.25,
        'bothtoscoreno' => 1.85,
        'awaytotalunder15' => 1.5,
        'win1' => 1.5,
        'handicap1minus1' => 1.6,
        'handicap2minus1' => 1.9,
        'handicap1plus1' => 1.2,
        'handicap2plus1' => 1.25,
        
    ];
    
    public static $betPriorities = [
        'Both to score no',
        'Total over 2.5',
        'Home team total over 1.5',
        'Away team to score yes',
        'Away team total under 1.5',
        'Home team to score yes',
        'Total over 1.5',
        'X1',
        'Total under 3.5', 
        'X2',
        'Win home team',
        'Win away team',
    ];

    public static $total = null;
    public static $pct = null;
    public static $coefWall = 1.75;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%result}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['team_id_home', 'team_id_away'], 'required'],
            [['team_id_home', 'team_id_away', 'goals_home', 'goals_away',], 'integer'],
            [['predict_goals_home', 'predict_goals_away'], 'double'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'team_id_home' => Yii::t('app', 'Team Id Home'),
            'team_id_away' => Yii::t('app', 'Team Id Away'),
            'goals_home' => Yii::t('app', 'Goals Home'),
            'goals_away' => Yii::t('app', 'Goals Away'),
            'predict_goals_home' => Yii::t('app', 'Predict Goals Home'),
            'predict_goals_away' => Yii::t('app', 'Predict Goals Away'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamIdHome()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id_home']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeamIdAway()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id_away']);
    }

    /**
     *
     * По количеству голов в матче и лиге возвращает рекомендуемы ставки
     *
     * @param $goalsHome хозяева забитые
     * @param $goalsAway гости забитые
     * @param $leagueID id лиги
     * @param string $for для чего функция, если для 'site', то возвращает html
     * для сайта, если 'api', то возвращает массив всех ставок
     */
    public static function getBets($goalsHome,$goalsAway, $leagueID, $for = 'site')
    {    
        $bets = [];
        $betPriorities = BetPriority::getBetBriorityByLeagueID($leagueID);
        
        foreach($betPriorities as $betPriority){
            switch($betPriority){
                case "Total over 2.5":
                    //ТБ 2.5
                    if($goalsHome + $goalsAway > 2.5){
                        array_push($bets, "Total goals: <b>over 2.5</b>");
                    }
                    break;
                case "Total over 1.5":
                    //ТБ 1.5
                    if($goalsHome + $goalsAway > 1.5){
                        array_push($bets, "Total goals: <b>over 1.5</b>");
                    }
                    break;
                case "Home team to score yes":
                    //Хозяева забьют да
                    if($goalsHome >= 1){
                        array_push($bets, "Home team to score: <b>Yes</b>");
                    }
                    break;
                case "Both to score no":
                    //обе забьют нет
                    if($goalsHome == 0 || $goalsAway == 0){
                        array_push($bets, "Both teams to score: <b>No</b>");
                    }   
                    break;
                case "Total under 3.5":
                    //ТМ 3.5
                    if($goalsHome + $goalsAway < 3.5){
                        array_push($bets, "Total goals: <b>under 3.5</b>");
                    }
                    break;
                case "Home team total over 1.5":
                   //Хозяева ТБ 1.5
                    if($goalsHome > 1.5){
                        array_push($bets, "Total goals home: <b>over 1.5</b>");
                    }
                    break;
                case "Away team to score yes":
                    //Гости забьют да
                    if($goalsAway >= 1){
                        array_push($bets, "Away team to score: <b>Yes</b>");
                    } 
                    break;
                case "Away team total under 1.5":
                    //Гости ТМ 1.5
                    if($goalsAway < 1.5){
                        array_push($bets, "Total goals away: <b>under 1.5</b>");
                    }
                    break;
                case "Win home team":
                   //Победа 1
                    if($goalsHome > $goalsAway){
                        array_push($bets, "Win: <b>Home team</b>");
                    } 
                    break;
                case "Win away team":
                    //Победа 2
                    if($goalsHome < $goalsAway){
                        array_push($bets, "Win: <b>Away team</b>");
                    }
                    break;
                case "X1":
                   //X1
                    if($goalsHome >= $goalsAway){
                        array_push($bets, "Win home team or draw: <b>X1</b>");
                    } 
                    break;
                case "X2":
                    //X2
                    if($goalsHome <= $goalsAway){
                        array_push($bets, "Win away team or draw: <b>X2</b>");
                    }
                    break;
            }
        }

        //если для api мобильного, то возвращаем массив со всеми ставками
        if($for == 'api') return $bets;

        return self::get3bets($bets);

    }
    
    public static function get3bets($bets){
        $res = "";
        if(count($bets) >= 1){
            $res = $bets[0];
        } 
        if(count($bets) >= 2){
            $res = $bets[0] . $bets[1];
        }
        if(count($bets) >= 3){
            $res = $bets[0] . $bets[1] . $bets[2];
        }
        if(count($bets) == 0){
            $res = "WOW...You have found unpredictable game...";
        }
        return $res;
    }
    
    public static function getAllBets($bets){
        $res = "";
        foreach($bets as $bet){
                $res .= "<div>$bet</div>";
            }
        return $res;
    }
    
    /**
     * Возвращает коэффициент и ставку по количествку голов хозяев, голов гостей, id лиги и моду
     * 
     * @param type $predictGoalsHome кол-во голов хозяев
     * @param type $predictGoalsAway кол-во голов гостей
     * @param type $leagueID id лили
     * @param type $mode мод, может быть optimize(при нажатии на optimize bet order, тогда приоритеты ставок берутся из 
     * Result::$betPriorities) или history(при нажатии на bet history, приоритеты ставок берутся исходя из лиги)
     * @return array массив из названия ставки и ее коэффициента
     */
    public static function getCoefAndBetName($predictGoalsHome, $predictGoalsAway, $leagueID, $mode){
        $betName = "No bet";
        $coef = 1;
        
        if($mode == "history"){
           $betPriorities = BetPriority::getBetBriorityByLeagueID($leagueID); 
        } elseif($mode == "optimize") {
            $betPriorities = Result::$betPriorities;
        }

        foreach($betPriorities as $betPriority){
            switch($betPriority){
                case "Total over 2.5":
                    //ТБ 2.5
                    if($predictGoalsHome + $predictGoalsAway > 2.5){
                        $betName = "Total over 2.5";
                        $coef = self::$coefs['totalover25'];
                        return [$betName, $coef];
                    }
                    break;
                case "Total over 1.5":
                    //ТБ 1.5
                    if($predictGoalsHome + $predictGoalsAway > 1.5){
                        $betName = "Total over 1.5";
                        $coef = self::$coefs['totalover15'];
                        return [$betName, $coef];
                    }
                    break;
                case "Home team to score yes":
                    //Хозяева забьют да
                    if($predictGoalsHome >= 1){
                        $betName = "Home team to score yes";
                        $coef = self::$coefs['hometoscoreyes'];
                        return [$betName, $coef];
                    }
                    break;
                case "Both to score no":
                    //обе забьют нет
                    if($predictGoalsHome == 0 || $predictGoalsAway == 0){
                        $betName = "Both to score no";
                        $coef = self::$coefs['bothtoscoreno'];
                        return [$betName, $coef];
                    }   
                    break;
                case "Total under 3.5":
                    //ТМ 3.5
                    if($predictGoalsHome + $predictGoalsAway < 3.5){
                        $betName = "Total under 3.5";
                        $coef = self::$coefs['totalunder35'];
                        return [$betName, $coef];
                    }
                    break;
                case "Home team total over 1.5":
                   //Хозяева ТБ 1.5
                    if($predictGoalsHome > 1.5){
                        $betName = "Home team total over 1.5";
                        $coef = self::$coefs['hometotal15'];
                        return [$betName, $coef];
                    }
                    break;
                case "Away team to score yes":
                    //Гости забьют да
                    if($predictGoalsAway >= 1){
                        $betName = "Away team to score yes";
                        $coef = self::$coefs['awaytoscoreyes'];
                        return [$betName, $coef];
                    } 
                    break;
                case "Away team total under 1.5":
                    //Гости ТМ 1.5
                    if($predictGoalsAway < 1.5){
                        $betName = "Away team total under 1.5";
                        $coef = self::$coefs['awaytotalunder15'];
                        return [$betName, $coef];
                    }
                    break;
                case "Win home team":
                   //Победа 1
                    if($predictGoalsHome > $predictGoalsAway){
                        $betName = "Win home team";
                        $coef = self::$coefs['win1'];
                        return [$betName, $coef];
                    } 
                    break;
                case "Win away team":
                    //Победа 2
                    if($predictGoalsHome < $predictGoalsAway){
                        $betName = "Win away team";
                        $coef = self::$coefs['win2'];
                        return [$betName, $coef];
                    }
                    break;
                case "X1":
                   //X1
                    if($predictGoalsHome >= $predictGoalsAway){
                        $betName = "X1";
                        $coef = self::$coefs['x1'];
                        return [$betName, $coef];
                    } 
                    break;
                case "X2":
                    //X2
                    if($predictGoalsHome <= $predictGoalsAway){
                        $betName = "X2";
                        $coef = self::$coefs['x2'];
                        return [$betName, $coef];
                    }
                    break;
            }
        }
     
        /*
        //Фора 1 -1
        if($predictGoalsHome - 1 > $predictGoalsAway){
            $betName = "Handicap home team -1";
            $coef = self::$coefs['handicap1minus1'];
            return [$betName, $coef];
        }
        //Фора 2 -1
        if($predictGoalsHome  < $predictGoalsAway - 1){
            $betName = "Handicap away team -1";
            $coef = self::$coefs['handicap2minus1'];
            return [$betName, $coef];
        }*/
         
        /*
        //Фора 1 +1
        if($predictGoalsHome + 1 > $predictGoalsAway){
            $betName = "Handicap home team +1";
            $coef = self::$coefs['handicap1plus1'];
            return [$betName, $coef];
        }
        //Фора 2 +1
        if($predictGoalsHome < $predictGoalsAway + 1){
            $betName = "Handicap away team +1";
            $coef = self::$coefs['handicap2plus1'];
            return [$betName, $coef];
        }*/
        return [$betName, $coef];
    }
    
    public static function checkWin($betName, $GH, $GA){
        switch($betName){
            case "Both to score no":
		return $GH == 0 || $GA == 0;
            case "Total over 2.5":
		return $GH + $GA > 2.5;
            case "Home team total over 1.5":
                return $GH > 1.5;
            case "Home team to score yes":
                return $GH >= 1;
            case "Away team to score yes":
                return $GA >= 1;
            case "Total over 1.5":
                return $GH + $GA > 1.5;
            case "Away team total under 1.5":
                return $GA < 1.5;
            case "Win home team":
                return $GH > $GA;
            case "Win away team":
                return $GH < $GA;
            default:
                return false;
        }
    }
    
    /**
     * Возвращает data provider для отображения тестов на истории
     * 
     * @param integer $leagueID id лиги
     * @param integer $seasonID id сезона
     * @param double $total общая начальная сумма
     * @param double $pct % от суммы, который ставим
     * @param string $mode может быть "history"(для тестов на истории, приоритет ставок в зависимости от лиги) 
     * или "optimize"(для оптимизации порядка приоритета ставок)
     * @return \yii\data\ArrayDataProvider
     */
    public static function getDPBetHistory1314($leagueID, $seasonID, $total, $pct, $mode){
        self::$total = $total;
        self::$pct = $pct;
        
        $DP = [];
        //находим только 30% игр конца сезона
        //находим все команды для данной лиги и сезона
        $subQueryTeams = (new Query())
                ->select('id')
                ->from('tbl_team')
                ->where('season_id = :season_id',[':season_id' => $seasonID])
                ->andWhere('league_id = :league_id',[':league_id' => $leagueID]);
        $countResults = Result::find()
                ->where(['team_id_home' => $subQueryTeams])
                ->orWhere(['team_id_away' => $subQueryTeams])
                ->count();
        $countResults -= round($countResults * (1 - self::$testPct));
        //находим 30% игр сезона
        $results = Result::find()
                ->where(['team_id_home' => $subQueryTeams])
                ->orWhere(['team_id_away' => $subQueryTeams])
                ->orderBy('id DESC')
                ->limit((integer)$countResults)
                ->all();
        //находим id начала и конца, для сортировки в нужнойм порядку
        $lowID = $results[0]->id;
        $highID = $results[0]->id;
        foreach($results as $result){
            if($result->id < $lowID){
                $lowID = $result->id;
            }
            if($result->id > $highID){
                $highID = $result->id;
            }
        }
        
        $results = Result::find()
                ->where('id >= :idLow',[':idLow' => $lowID])
                ->andWhere('id <= :idHigh',[':idHigh' => $highID])
                ->all();
        
        
        $countBets = 0;
        $totalCoef = 1;
        $bets = "";
        $betStatus = "win";
        
        foreach($results as $result){
            //голы
            $GH = $result->goals_home;
            $GA = $result->goals_away;
            //прогноз голов
            $pGH = number_format($result->predict_goals_home);
            $pGA = number_format($result->predict_goals_away);
            
            $betNameAndCoef = self::getCoefAndBetName($pGH, $pGA, $result->teamIdHome->league->id, $mode);
            $betName = $betNameAndCoef[0];
            $coef = $betNameAndCoef[1];
            
            $totalCoef *= $coef;
            $countBets++;
            $homeTeamLatin = Translator::getClubLatinName($result->teamIdHome->name);
            $awayTeamLatin = Translator::getClubLatinName($result->teamIdAway->name);
            $bets .= "$homeTeamLatin - $awayTeamLatin: $betName($GH-$GA); ";
            if(!self::checkWin($betName, $GH, $GA)){
                    $betStatus = "lose";
            }
            
            if($totalCoef < self::$coefWall){continue;}
            
            $sum = self::$total / 100 * self::$pct;
            if($betStatus == "win"){
                $win = $sum * $totalCoef;
                $status = 'win';
            } else {
                $win = 0;
                $status = 'lose';
            }
            self::$total = self::$total - $sum + $win;
            $DP[] = [
                'id' => $result->id,
                'type' => $countBets > 1 ? 'Express' : 'Single',
                'bets' => $bets,
                'sum' => number_format($sum,2),
                'coef' => number_format($totalCoef,2),
                'win' => number_format($win,2),
                'total' => number_format(self::$total,2),
                'status' => $betStatus,
            ];
            $countBets = 0;
            $bets = "";
            $totalCoef = 1;
            $betStatus = "win";
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $DP,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
        return $dataProvider;
    }
    
    /**
     * по массиву игр возвращает DataProvider с экспрессами для следующего тура
     * 
     * @param array $matches массив игр
     */
    public static function getDPTourBets($matches){
        $DP = [];
        $countBets = 0;
        $totalCoef = 1;
        $bets = "";
        $betStatus = "win";
        $mode = "history";
        $countMatches = count($matches);
        $i = 0;
        $arrIDres = [];
        $arrIDbet = [];
        foreach($matches as $match){
            $mHomeTeam = Team::findOne($match['team_id_home']);
            $mAwayTeam = Team::findOne($match['team_id_away']);
            //прогноз голов
            $pGH = number_format($match['predict_goals_home']);
            $pGA = number_format($match['predict_goals_away']);
            
            $betNameAndCoef = self::getCoefAndBetName($pGH, $pGA, $mHomeTeam->league->id, $mode);
            $betName = $betNameAndCoef[0];
            $coef = $betNameAndCoef[1];
            
            $totalCoef *= $coef;
            $countBets++;
            
            $homeTeamLatin = Translator::getClubLatinName($mHomeTeam->name);
            $awayTeamLatin = Translator::getClubLatinName($mAwayTeam->name);
            $bets .= "$homeTeamLatin - $awayTeamLatin: $betName; ";
            
            $arrIDres[] = $match['id'];
            $mBet = BetType::find()->where('name = :name',[':name' => $betName])->one();
            $arrIDbet[] = $mBet->id;
            
            $i++;
            if($totalCoef < self::$coefWall){
                if($i != $countMatches) { continue; }  
            }
            
            $DP[] = [
                'id' => $match['id'],
                'type' => $countBets > 1 ? 'Express' : 'Single',
                'bets' => $bets,
                'coef' => $totalCoef,
                'arrIDres' => $arrIDres,
                'arrIDbet' => $arrIDbet,
            ];
            $countBets = 0;
            $bets = "";
            $totalCoef = 1;
            $arrIDbet = [];
            $arrIDres = [];
        }
        $dataProvider = new ArrayDataProvider([
            'allModels' => $DP,
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);
        return $dataProvider;
    }
    
    //возвращает массив игр следующего тура
    public static function getNextTourGames($seasonID, $leagueID){
        $gamesCount = Stats::getGameCountInTour($seasonID, $leagueID);

        /**
         * смещение игр
         * к примеру если какой то матч перенесли, то должны выводится игры в след туре без несостоявшейся игры
         */
        $offset = 0;
        //если РФПЛ сезона 14/15 то убираем 1 несостоявшуюся игру
        if($seasonID == 2 && $leagueID == 2){
            $offset++;
            $offset++;
            $offset++;
        }
        //если чемпионат бельгии, то убираем 1 игру
//        if($seasonID == 2 && $leagueID == 11){
//            $offset++;
//        }
        //если УПЛ 14/15, то убираем 1 тур
        if($seasonID == 2 && $leagueID == 4){
            $offset = 7;
        }
        //если чемпионат испании, то убираем 1 игру
        if($seasonID == 2 && $leagueID == 7){
            $offset++;
        }
        
        $teamQuery = (new Query())
                ->select('id')
                ->from('tbl_team t')
                ->where('t.season_id = :season_id', [':season_id' => $seasonID])
                ->andWhere('t.league_id = :league_id', [':league_id' => $leagueID]);
        
        $rows = (new Query())
            ->select('*')
            ->from('tbl_result r')
            ->where(['r.team_id_home' => $teamQuery])
            ->andWhere(['r.goals_home' => null])
            ->limit($gamesCount + $offset)
            ->all();
        
        /**
         * если РФПЛ сезона 14/15 то убираем 1 несостоявшуюся игру
         */
        if($seasonID == 2 && $leagueID == 2){
            array_shift($rows);
            array_shift($rows);
            array_shift($rows);
        }
        /**
         * если чемпионат бельгии сезона 14/15 то убираем 1 несостоявшуюся игру
         */
//        if($seasonID == 2 && $leagueID == 11){
//            array_shift($rows);
//        }
        //убираем тур в УФПЛ
        if($seasonID == 2 && $leagueID == 4){
            for($i = 0; $i < $offset; $i++){
                array_shift($rows);
            }
        }
        /**
         * если чемпионат испании сезона 14/15 то убираем 1 несостоявшуюся игру
         */
        if($seasonID == 2 && $leagueID == 7){
            array_shift($rows);
        }
        
        return $rows;
    }
    
    //возвращает номер следующего тура
    public static function getNextTourNumber($seasonID, $leagueID){
        
        $offset = 0;
        //смещение по турам
        //если УФПЛ, то +1 тур
        if($seasonID == 2 && $leagueID == 4){
            $offset = 1;
        }
        
        $gamesCount = Stats::getGameCountInTour($seasonID, $leagueID);

        $teamQuery = (new Query())
                ->select('id')
                ->from('tbl_team t')
                ->where('t.season_id = :season_id', [':season_id' => $seasonID])
                ->andWhere('t.league_id = :league_id', [':league_id' => $leagueID]);
        
        $rowsNumber = (new Query())
            ->select('*')
            ->from('tbl_result r')
            ->where(['r.team_id_home' => $teamQuery])
            ->andWhere('!isnull(r.goals_home)')
            ->count();
        
        //return $rowsNumber;
        return round($rowsNumber / $gamesCount + 1) + $offset;
    }

    //по id команды возвращают количество игр из $gamesCount, в которых команда забивала
    public static function getScoreGamesCount($teamID, $gamesCount){

        $res = 0;

        $games = Result::find()
            ->where('(team_id_home = :teamID OR team_id_away = :teamID) AND !isnull(goals_home)',[':teamID' => $teamID])
            ->orderBy('id DESC')
            ->limit($gamesCount)
            ->all();

        foreach($games as $game){
            if($game->team_id_home == $teamID){
                if(number_format($game->goals_home) >= 1){
                    $res++;
                }
            } else {
                if(number_format($game->goals_away) >= 1){
                    $res++;
                }
            }
        }

        return $res;

    }

    //по id команды возвращают количество забитых мячей из кол-ва игр $gamesCount
    public static function getScoredGoalsCount($teamID, $gamesCount){

        $res = 0;

        $games = Result::find()
            ->where('(team_id_home = :teamID OR team_id_away = :teamID) AND !isnull(goals_home)',[':teamID' => $teamID])
            ->orderBy('id DESC')
            ->limit($gamesCount)
            ->all();

        foreach($games as $game){
            if($game->team_id_home == $teamID){
                $res += number_format($game->goals_home);
            } else {
                $res += number_format($game->goals_away);
            }
        }

        return $res;

    }

    //по id команды возвращают количество пропущенных мячей из кол-ва игр $gamesCount
    public static function getMissedGoalsCount($teamID, $gamesCount){

        $res = 0;

        $games = Result::find()
            ->where('(team_id_home = :teamID OR team_id_away = :teamID) AND !isnull(goals_home)',[':teamID' => $teamID])
            ->orderBy('id DESC')
            ->limit($gamesCount)
            ->all();

        foreach($games as $game){
            if($game->team_id_home == $teamID){
                $res += number_format($game->goals_away);
            } else {
                $res += number_format($game->goals_home);
            }
        }

        return $res;

    }

}
